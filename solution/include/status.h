#ifndef _STATUS_H
#define _STATUS_H

enum read_status {
	READ_OK = 0,
	READ_IO_ERROR,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER
};

enum write_status {
	WRITE_OK = 0,
	WRITE_ERROR,
};

extern const char *read_message[];
extern const char *write_message[];

#endif
