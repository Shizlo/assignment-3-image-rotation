#ifndef _TRANSFORM_H
#define _TRANSFORM_H

#include "img.h"

struct image rotate(const struct image *source);

#endif
