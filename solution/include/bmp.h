#ifndef _BMP_H
#define _BMP_H

#include <stdio.h>

#include "img.h"
#include "status.h"

#define BMP_BITMAPINFOHEADER 19778
#define BMP_OFFBITS sizeof(struct bmp_header)
#define BMP_NOTHING_RESERVED 0
#define BMP_INFO_HEADER_SIZE 40
#define BMP_COLOR_PLANES 1
#define BMP_NO_COMPRESSION 0
#define BMP_DUMMY_IMAGE_SIZE 0
#define BMP_UNDEFINED_RESOLUTION 0
#define BMP_EVERY_COLOR_USED 0
#define BMP_EVERY_COLOR_IMPORTANT 0

struct __attribute__((packed)) bmp_header {
	struct __attribute__((packed)) {
		uint16_t type;
		uint32_t size;
		uint32_t reserved;
		uint32_t offbits;
	} file;
	struct __attribute__((packed)) {
		uint32_t size;
		uint32_t width;
		uint32_t height;
		uint16_t planes;
		uint16_t bit_count;
		uint32_t compression;
		uint32_t size_image;
		uint32_t x_pixels_per_meter;
		uint32_t y_pixels_per_meter;
		uint32_t colors_used;
		uint32_t colors_important;
	} info;
};

enum read_status from_bmp(FILE *in, struct image *img);
enum write_status to_bmp(FILE *out, const struct image *img);

#endif
