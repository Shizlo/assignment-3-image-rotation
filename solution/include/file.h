#ifndef _FILE_H
#define _FILE_H

#include <stdint.h>
#include <stdio.h>

#include "status.h"

FILE *fopen_read(const char *filename);
FILE *fopen_write(const char *filename);

enum read_status fread_pad(void *buffer, uint64_t size, uint64_t n, FILE *f);
enum write_status fwrite_pad(const void *buffer, uint64_t size, uint64_t n, FILE *f);

inline uint64_t apply_padding(uint64_t x) {
	return (x + 3) >> 2 << 2;
}

#endif
