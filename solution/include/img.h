#ifndef _IMAGE_H
#define _IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
	uint8_t b, g, r;
};

struct image {
	uint64_t width, height;
	struct pixel *data;
};

struct image create_image(uint64_t width, uint64_t height);
void destroy_image(struct image *img);

inline struct pixel *pixel_at(const struct image *img, uint64_t left, uint64_t top) {
	return img->data + top * img->width + left;
}

#endif
