#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "file.h"
#include "transform.h"

int main(int argc, char **argv) {
	if (argc != 3) {
		if (argc) {
			fprintf(stderr, "try '%s [source file name] [destination file name]'\n", argv[0]);
		}
		exit(EXIT_FAILURE);
	}

	FILE *in = fopen_read(argv[1]);
	FILE *out = fopen_write(argv[2]);

	struct image img;
	enum read_status read_status = from_bmp(in, &img);

	fprintf(stderr, "%s\n", read_message[read_status]);
	if (read_status != READ_OK) {
		exit(EXIT_FAILURE);
	}

	struct image rotated = rotate(&img);

	fprintf(stderr, "Image rotated\n");

	enum write_status write_status = to_bmp(out, &rotated);

	fprintf(stderr, "%s\n", write_message[write_status]);
	if (read_status != READ_OK) {
		exit(EXIT_FAILURE);
	}

	fclose(in);
	fclose(out);

	destroy_image(&img);
	destroy_image(&rotated);

    return 0;
}
