#include "file.h"

#define READ "r"
#define WRITE "w"
#define BYTES "b"

FILE *fopen_read(const char *filename) {
	return fopen(filename, READ BYTES);
}

FILE *fopen_write(const char *filename) {
	return fopen(filename, WRITE BYTES);
}

enum read_status fread_pad(void *buffer, uint64_t size, uint64_t n, FILE *f) {
	size_t read_number = fread(buffer, size, n, f);
	if (read_number != n) {
		return READ_IO_ERROR;
	}

	size_t padding = apply_padding(size * n) - size * n;
	fseek(f, (long) padding, SEEK_CUR);

	return READ_OK;
}

enum write_status fwrite_pad(const void *buffer, uint64_t size, uint64_t n, FILE *f) {
	size_t write_number = fwrite(buffer, size, n, f);
	if (write_number != n) {
		return WRITE_ERROR;
	}

	size_t padding = apply_padding(size * n) - size * n;
	const static uint8_t padding_buffer[3] = {0};

	write_number = fwrite(padding_buffer, 1, padding, f);
	if (write_number != padding) {
		return WRITE_ERROR;
	}

	return WRITE_OK;
}

extern inline uint64_t apply_padding(uint64_t x);
