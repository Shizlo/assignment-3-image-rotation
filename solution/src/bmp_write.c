#include "bmp.h"

#include <stdbool.h>
#include <stddef.h>

#include "file.h"

static struct bmp_header create_header(const struct image *img) {
	uint64_t bits_size = img->height * apply_padding(img->width) * sizeof(struct pixel);
	return (struct bmp_header) {
		.file = {
			.type = BMP_BITMAPINFOHEADER,
			.size = BMP_OFFBITS + bits_size,
			.reserved = BMP_NOTHING_RESERVED,
			.offbits = BMP_OFFBITS
		},
		.info = {
			.size = BMP_INFO_HEADER_SIZE,
			.width = img->width,
			.height = img->height,
			.planes = BMP_COLOR_PLANES,
			.bit_count = sizeof(struct pixel) * 8,
			.compression = BMP_NO_COMPRESSION,
			.size_image = BMP_DUMMY_IMAGE_SIZE,
			.x_pixels_per_meter = BMP_UNDEFINED_RESOLUTION,
			.y_pixels_per_meter = BMP_UNDEFINED_RESOLUTION,
			.colors_used = BMP_EVERY_COLOR_USED,
			.colors_important = BMP_EVERY_COLOR_IMPORTANT
		}
	};
}

enum write_status to_bmp(FILE *out, const struct image *img) {
	struct bmp_header header = create_header(img);
	bool write_success = fwrite(&header, sizeof(header), 1, out);
	if (!write_success) {
		return WRITE_ERROR;
	}

	for (uint64_t row_num = 0; row_num < img->height; row_num++) {
		struct pixel *row_start = pixel_at(img, 0, row_num);
		enum write_status write_error = fwrite_pad(row_start, sizeof(struct pixel), img->width, out);

		if (write_error) {
			return WRITE_ERROR;
		}
	}

	return WRITE_OK;
}
