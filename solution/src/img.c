#include "img.h"

#include <stdio.h>

struct image create_image(uint64_t width, uint64_t height) {
	struct pixel *data = malloc(width * height * sizeof(struct pixel));
	if (data == NULL) {
		perror("Can't load image into memory");
		exit(EXIT_FAILURE);
	}

	return (struct image) {
		.width = width,
		.height = height,
		.data = data
	};
}

void destroy_image(struct image *img) {
	free(img->data);
	img->data = NULL;
}

extern inline struct pixel *pixel_at(const struct image *img, uint64_t left, uint64_t top);
