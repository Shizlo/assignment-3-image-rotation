#include "transform.h"

struct image rotate(const struct image *source) {
	struct image out = create_image(source->height, source->width);

	for (uint64_t row = 0; row < source->height; row++) {
		for (uint64_t col = 0; col < source->width; col++) {
			struct pixel *source_px = pixel_at(source, col, row);
			struct pixel *out_px = pixel_at(&out, source->height - row - 1, col);
			*out_px = *source_px;
		}
	}

	return out;
}
