#include "bmp.h"

#include <stdbool.h>

#include "file.h"

static enum read_status read_header(FILE *in, struct bmp_header *header) {
	bool read_error = !fread(&header->file, sizeof(header->file), 1, in);

	if (read_error) {
		return READ_INVALID_HEADER;
	}

	if (header->file.type != BMP_BITMAPINFOHEADER) {
		return READ_INVALID_SIGNATURE;
	};


	read_error = !fread(
		&header->info,
		sizeof(header->info), 1, in
	);

	if (read_error) {
		return READ_INVALID_HEADER;
	}

	fseek(in, (long) header->file.offbits, SEEK_SET);

	return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img) {
	struct bmp_header header;
	enum read_status header_read_error = read_header(in, &header);

	if (header_read_error) {
		return header_read_error;
	}

	*img = create_image(header.info.width, header.info.height);

	for (uint64_t row_num = 0; row_num < header.info.height; row_num++) {
		struct pixel *row_start = pixel_at(img, 0, row_num);
		enum read_status row_read_err = fread_pad(row_start, sizeof(struct pixel), header.info.width, in);
		if (row_read_err) {
			return READ_INVALID_BITS;
		}
	}

	return READ_OK;
}
