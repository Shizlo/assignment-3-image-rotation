#include "status.h"

const char *read_message[] = {
	[READ_OK] = "Image loaded",
	[READ_IO_ERROR] = "Couldn't read image file",
	[READ_INVALID_SIGNATURE] = "Not supported bitmap format",
	[READ_INVALID_BITS] = "File is corrupted",
	[READ_INVALID_HEADER] = "File is not a bmp"
};
const char *write_message[] = {
	[WRITE_OK] = "Image saved",
	[WRITE_ERROR] = "Couldn't save image"
};
